;;; dice.lisp
(defpackage :spaces_dice
	;; These have been casually copied from my other attempts at things,
	;; but i hope this can just become a coherent library eventually, and if so it will be renamed
	(:use :common-lisp
	)
	;; (:local-nicknames
		;; :pcg
	;; )
	(:export
		;; #::*prng*
		#:random-integer
		#:random-item
		#:random-item-weighted  ; #::equal-weight
		
))
(in-package :spaces_dice)


;; LATER: there may be times the prng should re-seed
(defparameter *prng* (pcg:make-pcg :seed (get-universal-time)))


(defun random-integer (min-value max-value) ; pick pseudo-random integer {{{
	;; this function actually never generates 'max-value, so you need to add 1
	(pcg:pcg-random *prng* min-value (+ max-value 1))
) ; }}}

(defun random-item (seq) ; pick item from sequence {{{
	(let ((roll
		(random-integer
			0
			(- (length seq) 1))
		))
		(elt seq roll)
)) ; }}}

(defun equal-weight (sequence-length) ; {{{
	(make-array sequence-length :initial-element 1)
) ; }}}

(defun random-item-weighted (items  &key index item-weights) ; pick item with some distribution {{{
;; doc - a little old {{{
"Choose an item at random from array ITEMS according to a given distribution, or if none given, with equal probability.

WEIGHT-FUNC - a function of x which returns the frequency of the item at that position."
;; }}}

	(let (integrals  last-weight (sum 0)  roll result-number)
	
	(cond ; if passed an integer, pick from integers within {{{
		((realp items)
			(setq
				;; allow non-integers but round them down
				items (floor items)
				;; you should pass :index 0 [default] or :index 1 as this returns a number
				index  (if (null index)  0  index)
				;; use the integer as the number of items
				last-weight  items)
		)
		(t  (setq last-weight (length items)))
	) ; }}}
	;; zero-index number of items
	(setq last-weight (- last-weight 1))
	
	(cond  ; fix item-weights if necessary {{{
		;; if item-weights is an array leave it alone
		((arrayp item-weights) '())
		;; if item-weights is a function, flatten it - i.e., crudely integrate the function
		;; this is necessary to roll the result number.
		((functionp item-weights)
			(setq integrals
				(make-array (+ last-weight 1) :initial-element 0))
			
			(loop for i from 0 to last-weight do
				(setf (aref integrals i)
					(floor (funcall item-weights i)))
			)
			(setq item-weights integrals)
		)
		;; if item-weights is not anything meaningful, pick with equal probability
		(t  (setq item-weights (equal-weight (+ last-weight 1)))
		)
	) ; }}}
	
	(setq  ; roll result {{{
		;; turn 'integrals into a sum of all the small integrals
		integrals  (reduce '+ item-weights)
		roll  ; generate number in the space of all items fully weighted
			(cond  ; but don't try to generate a number between 1~0 or 1~1
				((> integrals 1)  ; if integral is > 1 go ahead
					(random-integer 1 integrals)
					)
				((> integrals 0) 1)  ; if integral is 1 return 1
				(t  0))  ; if integral is less than 1 return 0
		) ; }}}
	
	;; look up item number that corresponds to weight roll 
	(loop for i  from 0 to last-weight  while (null result-number)  do ; {{{
		(let ((item-weight
			(aref item-weights i)
		))
		(setq sum  (+ item-weight sum))
		(when (<= roll sum)
			(setq result-number i))
	)) ; }}}
	
	;; if no result is chosen, assume you're probably picking an integer, and return 0
	(when (null result-number)  (setq result-number 0))
	
	(cond  ; return index of list, or list item {{{
		;; if not passed :index, return item in list
		((null index)  (elt items result-number))
		;; if passed :index 1, return a 1-indexed number
		((equal index 1)  (+ 1 result-number))
		;; if passed :index 0, or anything but nil, return a 0-indexed number
		(t  result-number)
		) ; }}}
)) ; }}}

;;; ns.lisp - this package does nothing but export symbols to use as xylem values
(defpackage :shardquest-xylem
	(:use :common-lisp)
	(:local-nicknames  (:x :xylem))
	(:export
		#:encounter-resident  ; used during 'encounter-resident
))

;;; first demo for shardquest - currently only a REPL/console 'text adventure'

;; in case you started the game from a terminal, make sure all asdf systems are loaded
(when (null (find-package :pcg))  (asdf:load-system "shardquest"))

(defpackage :shardquest
	(:shadowing-import-from :shardquest-map
		#:move-by #:move-x #:move-y
		#:visit-portal
		#:grid-status
		#:encounter-resident
		#:use-item
		)
	(:use :common-lisp)
	(:local-nicknames
		(:g :grevillea)
			;; printl debug-hash
		(:x :xylem)
		(:s :shardquest-xylem)  ; namespace for xylem values
		
		(:ext :shardquest-plugin)
		(:map :shardquest-map)
	)
	(:export
		;; imported from map
		#:move-by #:move-x #:move-y
		#:visit-portal
		#:encounter-resident
		#:use-item
		
		;; commands
		#:grid-status #:inventory #:ally-list
		
		#:begin
))
(in-package :shardquest)


;;; commands {{{

(defun inventory () ; describe inventory {{{
	(let ((item-list
		(x:xylem-value (list 'x:xylem-value 'map::inventory))
	))
	
	(format t "Inventory:~%")
	
	;; print quantities of each item
	(dolist (item-symbol item-list)
		(format t "~A × ~a~%"
			;; LATER: use gettext or something to localise item names
			(map:item-name (funcall item-symbol))
			(x:xylem-value (list 'map::inventory item-symbol))
			)
	)
	
	t
)) ; }}}

(defun ally-list () ; describe allies {{{
	(let ((item-list
		(x:xylem-value (list 'x:xylem-value 'map::allies))
	))
	
	;; print info about each ally
	(dolist (ally-symbol item-list)
		(format t "Ally ~a - ~a~%"
			;; LATER: describe actual ally data structures when there are any
			ally-symbol
			(x:xylem-value (list 'map::allies ally-symbol))
			)
	)
	
	t
)) ; }}}

;;; }}}


(defun begin () ; {{{
	(x:xylem-init)  ; initialise xylem table
	(let (dummy)
	dummy
	
	(g:load-extension "spaces_mons")
	
	(setq map:*grid* (map:random-grid))
	(grid-status)
	
	
	
	t
)) ; }}}
